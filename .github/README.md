<h1 align="center">NeoVim Configuration</h1>

<p align="left">
    <a href="" rel="noopener">
        <img  width="45%" height="45%" src="https://raw.githubusercontent.com/Thomashighbaugh/nvim/main/.github/assets/nvim.png" alt="Project logo">
    </a>
</p>

<br/>
<p align="right">
<p>Ok so I rewrote the entire thing over again using an even more complicated but powerful configuration as a base [by CreativeNull](https://github.com/CreativeNull/nvim-config). There are a few things now trimmed out of the configuration (namely note managers as I have succumbed to Notion.so's convenience and ease of use once more).</p>
</p>

<hr/>

## Screenshots

![dashboard](assets/dashboard.png)
![whichkey](assets/whichkey.png)
![neovimtree](assets/neovimtree.png)

## Install the Latest NeoVim

```
curl -o-  https://raw.githubusercontent.com/Thomashighbaugh/nvim/main/bin/install-latest-neovim.sh | bash
```

## Install this configuration

```
curl -o-  https://raw.githubusercontent.com/Thomashighbaugh/nvim/main/bin/install | bash
```

## Documentation

For more about the configuration, follow the links below to the documentation available in the repository's wiki.

- [About](https://github.com/Thomashighbaugh/nvim/wiki/About)
- [Installing](https://github.com/Thomashighbaugh/nvim/wiki/Installing)
- [Acknowledgements](https://github.com/Thomashighbaugh/nvim/wiki/Acknowledgements)
