local base_colors = {
  light = '#f4f4f7',
  dark = '#1b1d24',
  softlight = '#b2bfd9',
  softdark = '#555e70',
}

return {
  text_light = base_colors.light,
  text_dark = base_colors.dark,
  text_softlight = base_colors.softlight,
  text_softdark = base_colors.softdark,

  bg_light = base_colors.light,
  bg_dark = base_colors.dark,
  bg_softlight = base_colors.softlight,
  bg_softdark = base_colors.softdark,

  primary = '#323643',
  indigo = '#6c31f4',
  gray = '#323643',
  darkgray = '#272A34',

  insert_primary = '#44ffdd',
  visual_primary = '#ff3d81',
  command_primary = '#8b9cbe',
  replace_primary = '#8265ff',

  error = '#e91e63',
  warn = '#ffff35',
}
