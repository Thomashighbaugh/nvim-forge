local M = {
  plugins = {
    { 'numToStr/Comment.nvim', opt = true },
  },
}

return M
